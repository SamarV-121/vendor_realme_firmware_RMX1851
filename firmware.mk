# Firmware
PRODUCT_COPY_FILES += \
    vendor/realme/firmware/RMX1851/abl.img:install/firmware-update/abl.img \
    vendor/realme/firmware/RMX1851/xbl.img:install/firmware-update/xbl.img \
    vendor/realme/firmware/RMX1851/xbl_config.img:install/firmware-update/xbl_config.img \
    vendor/realme/firmware/RMX1851/dpAP.img:install/firmware-update/dpAP.img \
    vendor/realme/firmware/RMX1851/dtbo.img:install/firmware-update/dtbo.img \
    vendor/realme/firmware/RMX1851/tz.img:install/firmware-update/tz.img \
    vendor/realme/firmware/RMX1851/modem.img:install/firmware-update/modem.img \
    vendor/realme/firmware/RMX1851/DRIVER.img:install/firmware-update/DRIVER.img \
    vendor/realme/firmware/RMX1851/qupv3fw.img:install/firmware-update/qupv3fw.img \
    vendor/realme/firmware/RMX1851/oppo_sec.img:install/firmware-update/oppo_sec.img \
    vendor/realme/firmware/RMX1851/splash.img:install/firmware-update/splash.img \
    vendor/realme/firmware/RMX1851/devcfg.img:install/firmware-update/devcfg.img \
    vendor/realme/firmware/RMX1851/dpMSA.img:install/firmware-update/dpMSA.img \
    vendor/realme/firmware/RMX1851/BTFM.img:install/firmware-update/BTFM.img \
    vendor/realme/firmware/RMX1851/static_nvbk.img:install/firmware-update/static_nvbk.img \
    vendor/realme/firmware/RMX1851/dspso.img:install/firmware-update/dspso.img \
    vendor/realme/firmware/RMX1851/cmnlib.img:install/firmware-update/cmnlib.img \
    vendor/realme/firmware/RMX1851/cmnlib64.img:install/firmware-update/cmnlib64.img \
    vendor/realme/firmware/RMX1851/aop.img:install/firmware-update/aop.img \
    vendor/realme/firmware/RMX1851/storsec.img:install/firmware-update/storsec.img \
    vendor/realme/firmware/RMX1851/keymaster64.img:install/firmware-update/keymaster64.img \
    vendor/realme/firmware/RMX1851/hyp.img:install/firmware-update/hyp.img \
    vendor/realme/firmware/RMX1851/vendor.new.dat.br:install/firmware-update/vendor.new.dat.br \
    vendor/realme/firmware/RMX1851/vendor.transfer.list:install/firmware-update/vendor.transfer.list \
    vendor/realme/firmware/RMX1851/vendor.patch.dat:install/firmware-update/vendor.patch.dat
